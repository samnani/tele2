<?php

/**
 * Tele2_TemplateLibrary
 * Rendering of variables into template
 * It renders the variables assigned them through load method through main app.php file
 * 
 *
 *
 * @author Altaf Samnani
 * @version 1.0
 */
class Tele2_TemplateLibrary {

    /**
    *  Default Directory Path for View directory
    *
    * @var String
    */
    protected $templateDir = 'templates/';

    /**
    *  Set and get variable array for rendering in views
    *
    * @var Array
    */
    protected $vars = array ();

    /**
     * Constructor.
     * Assign the templates directory
     */
    public function __construct ($templateDir = NULL) {
        if ( $templateDir !== NULL ) {
            // Check here whether this directory really exists
            $this->templateDir = $templateDir;
        }
    }

    /**
     * Renders the file to display content
     * @param string $templateFile file to display
     *
     */
    public function render ($templateFile, $echo = TRUE) {

        if (file_exists ( $this->templateDir . $templateFile ) ) {
            if ( !$echo ) {
                ob_start(); // turn on output buffering
            }

            //Includes views file path for the tele2 assignment
            include $this->templateDir . $templateFile;
            
            if ( !$echo ) {
                
                $result = ob_get_contents( ); // get the contents of the output buffer
                
                ob_end_clean(); //  clean (erase) the output buffer and turn off output buffering
                
                return $result;
            }
        }
        else {
            throw new Exception ('no template file ' . $templateFile . ' present in directory ' . $this->templateDir);
        }

    }

    /**
     * PHP Magicmethod to assign variables
     * @param string $name name of the variable
     * @param string $value value of the variable
     *
     */
    public function __set ( $name, $value ) {

        $this->vars[$name] = $value;
    }

    /**
     * PHP Magicmethods to get variables value
     * @param string $name name of the variable
     *
     */
    public function __get ( $name ) {

         return ( isset( $this->vars[$name] ) ) ? $this->vars[$name] : NULL;
    }

    /**
     * PHP Magic method for checking if isset
     */
    public function __isset ( $key ) {

        return key_exists ( $key, $this->vars );
    }
}

?>
