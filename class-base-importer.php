<?php
/**
* Tele2_BaseImporter
* Common CSV to Mysql Importer Functions
*
* @access     public
* @copyright  Copyright (c) 2005-2015 Tele2
* @author     Altaf S
* @license    http://tele2.nl   TELE2
* @version    Release: 1
* @since      Class available since Release 1
* 
*/

/* 1 Include Base Importer Functions */
require_once( 'class-db-mapper.php' );

class Tele2_BaseImporter extends Tele2_DatabaseMapper {
   /**
    * Directory Path for CSV File tobe kept
    *
    * @var String
    */
    private $csvDir     =   'csv';

    /**
    * File upload allowed extension
    *
    * @var Array
    */
    private $allowedExt =   array('csv');

    /**
     * validateDbConnection 
     *
     * Validates database connection and creates mysql table if it doesnt exist for the import
     * @param Array Database connection parameters
     *
     * @return boolean returns the boolean value for the database connection
     * @access protected
     */
    protected function validateDbConnection( $params ) {
        if( $params[ 'host'] && $params[ 'dbase' ] && $params[ 'username' ] && $params[ 'password' ] ) {

            if( !$this->initDbConnection( $params ) ) {

                return false;
            }

        } else {

            return false;
        }

        $this->createTableIfNotExists( );

        return true;
    }

    /**
     * import - Move File to the directory and Formats it to prepare for the mysql import
     *
     * @param none
     *
     * @return Date returns current timestamp
     * @access protected
     */
    protected function import( ) {
        // formates the csv for mysql import
        $fomattedCsv =  $this->formatCsv($csv);

        // Import csv file into database
        return $this->importCsvToSql( $fomattedCsv );
    }

    /**
     * isFileValid 
     *
     * Checks if upload extension file is allowed or not
     * @param none
     *
     * @return string New file path
     * @access protected
     */
    protected function isFileValid( $fileName ) {
        $ext = pathinfo($fileName, PATHINFO_EXTENSION);

        if (!in_array($ext, $this->allowedExt)) {
            
            return false;

        } else {

            return true;
        }
    }

    /**
     * formatCsv
     *
     * Formats to solve for EOL issues
     * @param string $file_path
     *
     * @return string $file_path
     * @access private
     */
    private function formatCsv( ) {
        $csvPath    =   $_FILES["file"]["tmp_name"];

        //Load the file into a string
        $string = @file_get_contents($csvPath);

        if ( !$string ) {

            return $csvPath;
        }

        //Convert all line-endings using regular expression
        $string = preg_replace('~\r\n?~', "\n", $string);

        file_put_contents($csvPath, $string);

        return $csvPath;
    }

    /**
     * importCsvToSql
     * Import CSV into Mysql using LOAD DATA LOCAL INFILE function
     *
     *
     * @param $path
     * @return boolean if import is sucessfull or not
     */
    protected function importCsvToSql( $path ) {
        $query = sprintf( " LOAD DATA LOCAL INFILE '%s' INTO TABLE `$this->db`.`$this->table` 
                            FIELDS TERMINATED BY ';' 
                            LINES TERMINATED BY '\\n' 
                            IGNORE 1 LINES", addslashes( $path ), $this->table );

        //Calls Db Mapper Class Execute Sql function 
        if ( !$this->executeSql( $query ) ) {

            return false;
        }
        
        return true;
    }
}

?>