<?php
/**
* Tele2_DatabaseMapper
* Abstract Class DatabaseMapper for Sql related functions
*
* @access     public 
* @copyright  Copyright (c) 2005-2015 Tele2
* @author     Altaf S
* @license    http://tele2.nl   TELE2
* @version    Release: 1
* @since      Class available since Release 1
* 
*/

abstract class Tele2_DatabaseMapper {
	/**
    * Database connection
    *
    * @var String
    */
	protected $connection;

	/**
    * Database name
    *
    * @var String
    */
    protected $db;

    /**
    * Table name
    *
    * @var String
    */
    protected $table 	=   'tele2_users';

    /**
    * Table fields
    *
    * @var Arrau
    */
    private $fields     =   array( 'USERNAME'   =>  array( 'name'   => 'USERNAME', 'isnull' => 'NOT NULL', 'type'   => 'VARCHAR(45)' ),                         
                                   'POSTALCODE' =>  array( 'name'   => 'POSTALCODE', 'isnull' => 'NULL', 'type'   => 'VARCHAR(25)' ) );


    /**
     * getFields 
     * Gets the table fields
     *
     * @param none
     *
     * @return Array returns array of table fields
     * @access public
     *
     */
    public function getFields( ) {
        return $this->fields;
    }

    /**
     * setFields 
     * Sets the table fields
     *
     * @param none
     *
     * @return Array sets table fields
     * @access public
     *
     */
    public function setFields( $fields ) {

        $this->fields   =   $fields;

        return $this->fields;
    }


    /**
     * initDbConnection 
     *
     * Initializes database connection from the parameters
     * @param Array Database connection parameters
     *
     * @return boolean returns the boolean value for the database connection
     * @access protected
     */
	protected function initDbConnection( $dbParams ) {
    	// Database Hostname
        $host       =   $dbParams['host'];

        // Database Username
        $username   =   $dbParams['username'];

        // Database name
        $this->db   =   $dbParams['dbase'];

        // Mysql password
        if ( isset($dbParams['password'])) {

            $password   =   $dbParams['password'];
        } else {

            $password   =   '';
        }

        // Makes the daabase connection and assigin it to connection variable
        if (!$this->connection = mysqli_connect($host, $username, $password, $this->db) ) {

           //mysqli_error($this->connection); //Uncomment for debugging

           return false;
        }
        
        return true;
    }

    /**
     * createTableIfNotExists 
     *
     * Creates table into database if it doesnt exist It also creates unique index to avoid row duplication
     * @param none
     *
     * @return boolean returns the boolean value for table creation
     * @access protected
     */
    protected function createTableIfNotExists( ) {
        $fields     =   $this->getFields( );

        if( $fields ) {

            $queryFields    =   '';

            $indexFields    =   '';
            
            $sep            =   '';

            foreach( $fields as $field ) {

                $queryFields    .=   $sep . "`". $field['name']. "` ". $field['type']." ".$field['isnull'] ;

                $indexFields    .=   $sep . "`". $field['name']. "` ";

                $sep            =   ', ';
            }

            $createQuery    =   "CREATE TABLE IF NOT EXISTS `$this->db`.`$this->table` ( $queryFields,  CONSTRAINT `userpost_index` UNIQUE ( $indexFields ) ); " ;

            if (!$result = mysqli_query($this->connection, $createQuery)) {

               //exit(mysqli_error($this->connection)); //Uncomment for debugging

               return false;
            }
            
            return true;
        }
    }

    /**
     * executeSql 
     *
     * Executes sql query 
     * @param String sql query to be executed
     *
     * @return boolean returns the boolean value for query executed
     * @access protected
     */
    protected function executeSql( $query ) {
    	if (!$result = mysqli_query($this->connection, $query)) {

            //exit(mysqli_error($this->connection)); //Uncomment for debugging

            return false;
        }

        return true;
    }

    /**
     * Abstract function that needs to implemented in the derived class
     *
     *
     * @param none
     *
     * @return 
     * @access protected
     *
     */
	abstract protected function importCsvToSql( $path );

    

}