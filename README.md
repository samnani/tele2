# README #

## Author - Altaf Samnani ##

### Technologies Used  

1) PHP
2) MYSQL
3) CLONE GIT Repo(To Demonstrate my GIT skills )
   git clone https://samnani@bitbucket.org/samnani/tele2.git

### How to execute the script ?
Ex http://path-to-code/importer.php   

### Important Note

1) PHP Script creates table tele2_users If it doesnt exist. It also creates it with UNIQUE INDEX so there is no row duplication, Dont need to create table manually. 

2) For the assignment, guideline is  "It is possible that in the future, more fields will be added to the CSV format.". To make it possible in class-db-mapper.php introduced getFields/setFields PUBLIC functions that can be used to overwrite the fields. 

Object Oriented Programming Concepts have been used Ex Used Inheritance Base and Derived Class, Abstract Class & Methods Declaration in Base Class and Implementation in Derived Class

#### Controller - Importer.php 
It is handling submitted request, processing the data returned and assigning the variables and loading Views

#### Model - class-db-mapper.php
It is serving as mysql layer which is fetching the data and returnning it to class-base-importer to use it. 

#### View - view-importer.phtml
It displays templates and rendering the variables that are assigned through Controller. 

### Setup & File Structure

#### 1) Configure Apache

   Add entry to sites-available
   Enable site ex a2ensite tele2
   Add entry to /etc/hosts

#### 2) File Structure
   2.1) Entry point importer.php
   Assignment Entry point is importer.php file

   2.2) CSV Validation and basic Function stays in class-base-importer.php
   Assignment basic commons validation and other functions are stored in this file 

   2.3) class-db-mapper.php
   It Initializes database connection, set fields, create table and executes final import query, all mysql related functions should go in this file.

   2.3) Setup rendering views through templatelibrary.php & Rendering views through templates/view-importer.phtml  
   Assignment render views are setup through templatelibrary.php and views are rendered through view-importer.phtml file


### Assignment Explanation 

#### importer.php
  It triggers necessary function calls to validate and process the csv and mysql and it assigns the variables to the view for the rendering through view-importer.phtml

#### class-base-importer.php
   It contains actual implementation of validation, processesing the csv and initializing all the necessary functions and variables to prepare and execute mysql import. It also implements the body of ABSTRACT method importCsvToSql of class-db-mapper.php file

#### class-db-mapper.php
   It has all sql related functionalities, INITIALIZES the database connection, CREATES the TABLE, EXECUTES the MYSQL queries, it has ABSTRACT METHOD DECLARATION importCsvToSql which should be implemented in DERIVED class. 

#### templates/view-importer.phtml
   It renders the variable messages and html for the assignment, We are using responsive framework bootstrap and to demonstrate my skills have used Bootstrap Grid system Ex col-xs-*


