<?php

/**
* ImporterController
*
* Initializes Import from CSV to Mysql 
* @access     public
* @copyright  Copyright (c) 2005-2015 Tele2
* @author     Altaf S
* @license    http://tele2.nl   TELE2
* @version    Release: 1
* @since      Class available since Release 1
* 
*/

/* 1 Include Base Importer Functions */
require_once( 'class-base-importer.php' );

/* 2 Include Template Rendering Library */
require_once( 'templatelibrary.php' );

class ImporterController extends Tele2_BaseImporter {
    /**
    * Assigning view variables for the rendering
    *
    * @var Array
    */
    private $view;

    /**
     * Constructor.
     * Creates Database Validation, Connection and Creates Table if it doesnt exist
     *
     * @access public
     *
     */
    function __construct( ) {

         /* Initializes the template view object*/
        $this->view = new Tele2_TemplateLibrary( );

        /* If data is posted then process the submitted CSV*/
        if ( isset( $_POST['submit'] ) ) {

            $submittedParams    =   $_POST; 

            if ( !$this->validateDbConnection( $_POST ) ) {
            
                $this->view->error = 'Please check your Database Credentials or Hostname';
            }
        }
    }

    /**
     * initImport
     * Validates the CSV and Imports to Mysql 
     *
     * 
     * @return void
    */
    public function initImport( ) {
        /* If data is posted then process the submitted CSV*/
        if ( isset( $_POST['submit'] ) && !$this->view->error ) { 

            // Check if form submitted a file
            $fileName = $_FILES['file']['name'];

            if ( $fileName ) {
                // You wish to do file validation at this point
                if ( $this->isFileValid( $fileName ) ) {

                    // Import our csv file
                    if ( $this->import( ) ) {
                        // Provide success message to the user
                        $this->view->success = 'Congratulations, Your CSV has been successfully imported to mysql';
                    } else {
                        $this->view->error = 'Try again, CSV is not imported, Sorry for inconvenience';
                    }

                } else {
                    // Returns error if its not a CSV file
                    $this->view->error = 'You should upload a CSV file';
                }
                
            } else {
                // Returns error if its not a CSV file
                $this->view->error = 'Please upload the valid CSV file';
            }
        }
        /* Render the html and pass the $view variable that can be accessd through $this in phtml*/
        $this->view->render( 'view-importer.phtml' );

        return;
    }
}

/* Initialized Importer Controller Object */
$importerObject = new ImporterController();
 
 /* Call Importer Controller function to process the csv and import to mysql*/
$importerObject->initImport();

?>